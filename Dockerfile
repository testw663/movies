#Build jar
FROM gradle:jdk17 AS gradle
WORKDIR /app
COPY . .
RUN gradle bootJar

# Build rest of image
FROM openjdk:17 as runtime
WORKDIR /app
ENV PORT 8080
COPY --from=gradle /app/build/libs/*.jar /app/app.jar
RUN chown -R 1000:1000 /app
USER 1000:1000
ENTRYPOINT ["java", "-jar", "-Dserver.port=${PORT}", "app.jar"]
#ENTRYPOINT ["java", "-jar", "app.jar"]