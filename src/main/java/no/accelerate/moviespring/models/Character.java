package no.accelerate.moviespring.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 500, nullable = false)
    private String name;
    @Column(length = 20, nullable = false)
    private String alias;
    @Column(length = 10, nullable = false)
    private String gender;
    @Column(length = 100, nullable = true)
    private String picture;







}