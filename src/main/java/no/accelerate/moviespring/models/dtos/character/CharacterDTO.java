package no.accelerate.moviespring.models.dtos.character;

import lombok.Data;

@Data
public class CharacterDTO {
    private int id;
    private String name;
    private String alias;
    private String gender;
    private String picture;
}

