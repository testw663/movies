package no.accelerate.moviespring.models.dtos.franchise;

import lombok.Data;

@Data
public class FranchiseDTO {
    private int id;
    private String name;
    private String description;
}
