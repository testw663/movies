package no.accelerate.moviespring.models.dtos.movie;

import lombok.Data;
import no.accelerate.moviespring.models.Franchise;

@Data
public class MovieDTO {
    private int id;
    private String title;
    private String genre;
    private String release_year;
    private String director;
    private String picture;
    private String trailer;
    private Franchise franchise;
}