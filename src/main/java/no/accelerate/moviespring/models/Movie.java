package no.accelerate.moviespring.models;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * Domain class (entity) to represent a Project.
 * Includes an auto generated key and some validation.
 * Relationships are configured as default, so collections are lazily loaded.
 */
@Entity
@Getter
@Setter

public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false, length = 500)
    private String title;
    @Column(nullable = false, length = 255)
    private String genre;
    @Column(nullable = false)
    private int release_year;
    @Column(nullable = false, length = 100)
    private String director;
    @Column(nullable = true, length = 100)
    private String picture;
    @Column(nullable = true, length = 255)
    private String trailer;
    //@OneToOne(mappedBy = "franchise")
    @ManyToOne
    @JoinColumn(name = "id_franchise")
    @JsonBackReference
    private Franchise franchise;
    @ManyToMany
    @JoinTable(
            name = "movie_character",
            joinColumns = {@JoinColumn(name = "id_movie")},
            inverseJoinColumns = {@JoinColumn(name = "id_character")}
    )
    private Set<Character> characters;

}
