package no.accelerate.moviespring.services.movie;

import no.accelerate.moviespring.exceptions.MovieNotFoundException;
import no.accelerate.moviespring.models.Movie;
import no.accelerate.moviespring.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class MovieServiceImpl implements MovieService {

    private final MovieRepository movieRepository;

    public MovieServiceImpl(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @Override
    public Movie findById(Integer id) {
        return movieRepository.findById(id)
                .orElseThrow(() -> new MovieNotFoundException(id));
        //return null;
    }

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
        //return null;
    }

    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
        //return null;
    }

    @Override
    public void deleteById(Integer integer) {

    }

    @Override
    public boolean exists(int id) {
        return movieRepository.existsById(id);
    }

}
