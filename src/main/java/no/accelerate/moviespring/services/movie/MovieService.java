package no.accelerate.moviespring.services.movie;

import no.accelerate.moviespring.models.Movie;
import no.accelerate.moviespring.services.CrudService;
/**
 * Service for the Movie domain class.
 * Providing basic CRUD functionality through CrudService and any extended functionality.
 */
public interface MovieService extends CrudService<Movie, Integer> {
    boolean exists(int id);
}
