package no.accelerate.moviespring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class MovieSpringApplication {

    public static void main(String[] args) {

        SpringApplication.run(MovieSpringApplication.class, args);

    }
        //System.out.println("Movie title=" + movie.getAll());
        /*@GetMapping("/hello")
        public String sayHello(@RequestParam(value = "myName", defaultValue = "World") String name) {
            return String.format("Hello %s!", name);
        }

         */

}
