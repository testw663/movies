package no.accelerate.moviespring.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.accelerate.moviespring.mappers.MovieMapper;
import no.accelerate.moviespring.models.Movie;
import no.accelerate.moviespring.models.dtos.movie.MovieDTO;
import no.accelerate.moviespring.services.movie.MovieService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path="api/v1/movies")
public class MovieController {
    private final MovieService movieService;
    private final MovieMapper movieMapper;
    //Constructor
    public MovieController(MovieService movieService, MovieMapper movieMapper) {

        this.movieService = movieService;
        this.movieMapper = movieMapper;
    }
    @Operation(summary = "Get all movies")
    @GetMapping
    public ResponseEntity getAll() {
        Collection<MovieDTO> movs = movieMapper.movieToMovieDto(
                movieService.findAll()
        );
        return ResponseEntity.ok(movs);
    }

    @Operation(summary = "Get movie with Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping("{id}")
    public ResponseEntity<Movie> findById(@PathVariable int id) {
        return ResponseEntity.ok(movieService.findById(id));
    }

    @Operation(summary = "Add a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully added",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @PostMapping
    public ResponseEntity add(@RequestBody Movie movie) {
        Movie newMovie = movieService.add(movie);
        URI uri = URI.create("movies/" + newMovie.getId());
        //return ResponseEntity.status(HttpStatus.CREATED).build();
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Update a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
            description = "Movie successfully updated",
            content = @Content),
            @ApiResponse(responseCode = "400",
            description = "Malformed request", content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class)) }),

            @ApiResponse(responseCode = "404",
            description = "Movie not found with supplied ID", content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @PutMapping("/{id}")
    public ResponseEntity update(@RequestBody Movie movie, @PathVariable int id) {
        if(movie.getId() != id)
            return ResponseEntity.badRequest().build();
        movieService.update(movie);
        return ResponseEntity.noContent().build();
    }
}
