package no.accelerate.moviespring.mappers;

import no.accelerate.moviespring.models.Franchise;
import no.accelerate.moviespring.models.dtos.franchise.FranchiseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface FranchiseMapper {
    FranchiseDTO franchiseToFranchiseDto(Franchise franchise);
    // Mapping the other way
    @Mapping(target = "movies", ignore = true)
    @Mapping(target = "characters", ignore = true)
    Franchise franchiseDtoToFranchise(FranchiseDTO franchiseDTO);

}
