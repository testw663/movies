package no.accelerate.moviespring.mappers;

import no.accelerate.moviespring.models.Movie;
import no.accelerate.moviespring.models.dtos.movie.MovieDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;

@Mapper(componentModel = "spring")
public interface MovieMapper {
    MovieDTO movieToMovieDto(Movie movie);
    // Mapping the other way
    @Mapping(target = "franchise", ignore = true)
    @Mapping(target = "characters", ignore = true)
    Movie movieDtoToMovie(MovieDTO movieDTO);

    public abstract Collection<MovieDTO> movieToMovieDto(Collection<Movie> movies);

}
