package no.accelerate.moviespring.repositories;

import java.util.List;

public interface CrudRepository<T,ID> {
    List<T> findAll();
    T findById(ID id);
    int insert(T object);
    int update(T object);
}
