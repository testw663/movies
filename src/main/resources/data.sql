-- Delete tables
drop table if exists movie_character;

drop table if exists character;

drop table if exists movie;

drop table if exists franchise;

-- 01_tableCreate.sql
CREATE TABLE Character (
                           Id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
                           Name VARCHAR(500) NOT NULL,
                           Alias VARCHAR(20) NOT NULL,
                           Gender VARCHAR(10) NOT NULL,
                           Picture VARCHAR(100)
);

CREATE TABLE Franchise(
                          Id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
                          Name VARCHAR(100) NOT NULL,
                          Description VARCHAR(300)
);

CREATE TABLE Movie(
                          Id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
                          Title VARCHAR(500) NOT NULL,
                          Genre VARCHAR(255) NOT NULL,
                          Release_year INT NOT NULL,
                          Director VARCHAR(100) NOT NULL,
                          Picture VARCHAR(100),
                          Trailer VARCHAR(255),
                          id_Franchise INT REFERENCES Franchise
);

CREATE TABLE Movie_Character(
                                id_Movie int REFERENCES Movie,
                                id_Character int REFERENCES Character,
                                PRIMARY KEY (id_Movie, id_Character)
);

-- Characters
INSERT INTO Character ("name", "alias", "gender", "picture") VALUES ('Elijah Wood','EW','Male',null); -- 1
INSERT INTO Character ("name", "alias", "gender", "picture") VALUES ('Liv Tyler','LT','Female',null); -- 1
INSERT INTO Character ("name", "alias", "gender", "picture") VALUES ('Jack Nicholson','JN','Male',null); -- 1

-- Franchises
INSERT INTO Franchise ("name", "description") VALUES ('Middle-earth Enterprises', 'Formerly known as Tolkien Enterprises');
INSERT INTO Franchise ("name", "description") VALUES ('Fantasy Films', 'Fantasy Films');
INSERT INTO Franchise ("name", "description") VALUES ('Pando Company Inc', 'Also Raybert Productions');

-- Movies
INSERT INTO Movie ("title", "genre", "release_year", "director", "id_franchise") VALUES ('Lord of the Rings - The fellowship of the Ring','Fantasy',2001,'Peter Jackson', 1);
INSERT INTO Movie ("title", "genre", "release_year", "director", "id_franchise") VALUES ('One Flew Over the Cuckoo''s Nest','Horror',1975,'Miloš Forman', 2);
INSERT INTO Movie ("title", "genre", "release_year", "director","id_franchise") VALUES ('Easy Rider','Roadmovie',1969,'Dennis Hopper', 3);

-- Characters in movies
INSERT INTO Movie_Character (id_Movie, id_Character) VALUES (1,1);
INSERT INTO Movie_Character (id_Movie, id_Character) VALUES (1,2);
INSERT INTO Movie_Character (id_Movie, id_Character) VALUES (2,3);
INSERT INTO Movie_Character (id_Movie, id_Character) VALUES (3,3);

